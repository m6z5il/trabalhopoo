CREATE DATABASE primatech
USE primatech

CREATE TABLE prod(
modelo CHAR(15) NOT NULL,
especs CHAR(250),
tela INT,
processador CHAR(10),
memoria CHAR(10),
so CHAR(10),
valor INT NOT NULL,
tipo CHAR(10),
bateria CHAR(15),
camera CHAR(10),
marca CHAR(20)
PRIMARY KEY(modelo)
)

select * from prod