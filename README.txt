O software apresentar� duas op��es logo de cara: Cadastrar Produto e Estoque.

Caso o usu�rio escolha "Cadastrar Produto", ele ir� ser mandado para uma tela 
com v�rios campos � serem preenchidos. OBS: os campos "Tela" e "Valor" s�
aceitam valores num�ricos.

	-Depois de preencher os dados, o usu�rio poder� selecionar a op��o "Cadastrar",
	onde ter� todos os dados cadastrados em um banco de dados. OBS: O software s� 
	ir� cadastrar caso n�o aja valores em branco.

	-Depois de preencher os dados, o usu�rio poder� selecionar a op��o "Editar",
	onde ele ter� que colocar todos os dados do modelo, os que quer alterar e os
	que n�o quer alterar.

	-Depois de preencher os dados, o usu�rio poder� selecionar a op��o "excluir",
	onde ele ter� que colocar apenas a op��o modelo para exclui-lo.

Caso o usu�rio escolha "Estoque", ele ser� levado a uma tela onde precisar� colocar
o modelo que quer consultar.