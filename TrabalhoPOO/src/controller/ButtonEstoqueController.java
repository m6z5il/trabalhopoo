package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import model.Produto;
import persistence.IProdutoDao;
import persistence.ProdutoDao;
import view.TelaConsultaProduto;
import view.TelaPrincipal;

public class ButtonEstoqueController implements ActionListener {

	private JButton btnHome, btnConsultar;
	private JFrame frmProduto;
	private JTextField tfProduto;

	public ButtonEstoqueController(JButton btnHome, JButton btnConsultar, JTextField tfProduto, JFrame frmProduto) {
		this.btnHome = btnHome;
		this.btnConsultar = btnConsultar;
		this.tfProduto = tfProduto;
		this.frmProduto = frmProduto;
	}

	public void actionPerformed(ActionEvent e) {
		Produto p = new Produto();
		if (e.getSource() == btnHome) {
			montaTelaHome();
		} else if (e.getSource() == btnConsultar) {
			p.setModelo(tfProduto.getText());
			consulta(p);
			montaTelaConsulta(p);
		}
	}

	private void consulta(Produto p) {
		IProdutoDao pDao = new ProdutoDao();
		try {
			pDao.consultaProduto(p);
			JOptionPane.showMessageDialog(null, "Consulta feita com sucesso", "Sucesso",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
		}

	}

	public void montaTelaHome() {
		frmProduto.dispose();
		TelaPrincipal hm = new TelaPrincipal();
		hm.frmTelaPrincipal.setVisible(true);
	}

	private void montaTelaConsulta(Produto p) {
		TelaConsultaProduto telaCP = new TelaConsultaProduto(p);
		telaCP.frmConsultaProduto.setVisible(true);
	}

}
