package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JRadioButton;

import view.TelaEstoque;
import view.TelaProduto;

public class RadioHomeController implements ActionListener {

	JRadioButton rdbtnEstoque, rdbtnProduto;
	JFrame frmTelaPrincipal;

	public RadioHomeController(JRadioButton rdbtnProduto, JRadioButton rdbtnEstoque, JFrame frmTelaPrincipal) {
		this.rdbtnProduto = rdbtnProduto;
		this.rdbtnEstoque = rdbtnEstoque;
		this.frmTelaPrincipal = frmTelaPrincipal;
	}

	public void actionPerformed(ActionEvent e) {
		if (rdbtnProduto.isSelected()) {
			montaTelaProduto();
		} else if (rdbtnEstoque.isSelected()){
			montaTelaEstoque();
		}

	}

	private void montaTelaEstoque() {
		frmTelaPrincipal.dispose();
		TelaEstoque telaE = new TelaEstoque();
		telaE.frmEstoque.setVisible(true);
	}

	private void montaTelaProduto() {
		frmTelaPrincipal.dispose();
		TelaProduto telaP = new TelaProduto();
		telaP.frmProduto.setVisible(true);
	}

}
