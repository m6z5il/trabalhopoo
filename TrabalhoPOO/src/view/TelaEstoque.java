package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import controller.ButtonEstoqueController;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TelaEstoque {

	public JFrame frmEstoque;
	private JTextField tfProduto;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaEstoque window = new TelaEstoque();
					window.frmEstoque.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaEstoque() {
		initialize();
	}

	private void initialize() {
		frmEstoque = new JFrame();
		frmEstoque.setResizable(false);
		frmEstoque.setTitle("Estoque");
		frmEstoque.setBounds(100, 100, 231, 120);
		frmEstoque.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEstoque.getContentPane().setLayout(null);

		JLabel lblProduto = new JLabel("Produto");
		lblProduto.setBounds(22, 30, 50, 14);
		frmEstoque.getContentPane().add(lblProduto);

		tfProduto = new JTextField();
		tfProduto.setBounds(92, 27, 115, 20);
		frmEstoque.getContentPane().add(tfProduto);
		tfProduto.setColumns(10);

		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.setBounds(121, 58, 89, 23);
		frmEstoque.getContentPane().add(btnConsultar);

		JButton btnHome = new JButton("Home");
		btnHome.setBounds(22, 58, 89, 23);
		frmEstoque.getContentPane().add(btnHome);
		
		ButtonEstoqueController btnEscon  = new ButtonEstoqueController(btnHome,btnConsultar, tfProduto, frmEstoque);
		
		btnConsultar.addActionListener(btnEscon);
		btnHome.addActionListener(btnEscon);

	}
}
