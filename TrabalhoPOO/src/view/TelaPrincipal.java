package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JRadioButton;

import controller.RadioHomeController;

public class TelaPrincipal {

	public JFrame frmTelaPrincipal;


	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal window = new TelaPrincipal();
					window.frmTelaPrincipal.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "ERRO, Tela n�o montada!");
				}
			}
		});
	}

	
	public TelaPrincipal() {
		initialize();
	}

	
	private void initialize() {
		frmTelaPrincipal = new JFrame();
		frmTelaPrincipal.setResizable(false);
		frmTelaPrincipal.setBounds(100, 100, 218, 156);
		frmTelaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTelaPrincipal.getContentPane().setLayout(null);

		JLabel lblHome = new JLabel("Home");
		lblHome.setFont(new Font("OCR A Extended", Font.PLAIN, 27));
		lblHome.setBounds(62, 11, 72, 52);
		frmTelaPrincipal.getContentPane().add(lblHome);

		JRadioButton rdbtnProduto = new JRadioButton("Cadastrar Produto");
		rdbtnProduto.setBounds(10, 70, 169, 23);
		frmTelaPrincipal.getContentPane().add(rdbtnProduto);

		JRadioButton rdbtnEstoque = new JRadioButton("Estoque");
		rdbtnEstoque.setBounds(10, 96, 109, 23);
		frmTelaPrincipal.getContentPane().add(rdbtnEstoque);

		RadioHomeController rhCon = new RadioHomeController(rdbtnProduto, rdbtnEstoque, frmTelaPrincipal);
		rdbtnProduto.addActionListener(rhCon);
		rdbtnEstoque.addActionListener(rhCon);

	}
}
